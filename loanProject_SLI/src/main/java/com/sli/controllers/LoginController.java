package com.sli.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

	@RequestMapping(value = "/signin")
	public String signOn(ModelMap model)
	{
		String someStr = "I come in peace from the signOn controller!!!";
		model.addAttribute("message", someStr);
		return "login";
	}
}
